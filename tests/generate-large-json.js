import fs from 'fs';

// Function to generate random nodes and edges
function generateGraphData(nodeCount, edgeCount) {
  let nodes = [];
  let edges = [];

  // Generate nodes
  for (let i = 0; i < nodeCount; i++) {
    nodes.push({
      id: i.toString(),
      label: 'Node ' + i,
      size: Math.random(),
      color: `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(
        Math.random() * 255,
      )}, ${Math.floor(Math.random() * 255)})`,
    });
  }

  // Generate edges
  for (let i = 0; i < edgeCount; i++) {
    edges.push({
      id: i.toString(),
      source: Math.floor(Math.random() * nodeCount).toString(),
      target: Math.floor(Math.random() * nodeCount).toString(),
      size: Math.random(),
      color: `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(
        Math.random() * 255,
      )}, ${Math.floor(Math.random() * 255)})`,
    });
  }

  return { nodes, edges };
}

// Generate sample data
const graphData = generateGraphData(700, 1400); // You can adjust the number of nodes and edges

// Write data to a JSON file
fs.writeFile('graphData.json', JSON.stringify(graphData, null, 2), (err) => {
  if (err) throw err;
  console.log('Data written to file');
});
