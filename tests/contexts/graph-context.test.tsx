import * as renderer from 'react-test-renderer';
import { GraphContext, graphReducer } from '../../src/contexts/graph-context';
import { GraphState } from '../../src/models/graph-types';
import { act } from 'react-dom/test-utils';

describe('GraphContext', () => {
  it('provides initial state', () => {
    const initialState: GraphState = {
      nodes: [],
      edges: [],
    };
    const testRenderer = renderer.create(
      <GraphContext.Provider
        value={{ graphState: initialState, dispatch: () => null }}
      >
        <GraphContext.Consumer>
          {({ graphState }) => <span>{graphState.nodes.length}</span>}
        </GraphContext.Consumer>
      </GraphContext.Provider>,
    );

    const testInstance = testRenderer.root;
    expect(testInstance.findByType('span').children).toEqual(['0']);
  });

  it('handles SET_GRAPH_DATA action', () => {
    const state = { nodes: [], edges: [] };
    const action = {
      type: 'SET_GRAPH_DATA',
      payload: {
        nodes: [
          { id: '1', label: 'Node 1', color: 'blue', size: 10 },
          { id: '2', label: 'Node 2', color: 'red', size: 20 },
        ],
        edges: [
          { id: '1', source: 'N 1', target: 'N 2', color: 'blue', size: 10 },
          { id: '2', source: 'N 2', target: 'N 1', color: 'red', size: 20 },
        ],
      },
    };

    let newState;
    act(() => {
      newState = graphReducer(state, action);
    });
    expect(newState).toEqual({
      nodes: [
        { id: '1', label: 'Node 1', color: 'blue', size: 10 },
        { id: '2', label: 'Node 2', color: 'red', size: 20 },
      ],
      edges: [
        { id: '1', source: 'N 1', target: 'N 2', color: 'blue', size: 10 },
        { id: '2', source: 'N 2', target: 'N 1', color: 'red', size: 20 },
      ],
    });
  });
});
