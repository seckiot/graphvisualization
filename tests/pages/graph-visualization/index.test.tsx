import * as renderer from 'react-test-renderer';
import GraphVisualization from '../../../src/pages/graph-visualization/index';

describe('GraphVisualization component', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<GraphVisualization />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
