import { forceGraph } from '../../../src/utils/graph/force-graph';
import * as d3 from 'd3';

describe('forceGraph', () => {
  it('should create a force graph with default parameters', () => {
    const nodes = [{ id: 'node1' }, { id: 'node2' }];
    const edges = [{ source: 'node1', target: 'node2' }];
    const svg = d3.create('svg');

    forceGraph({ nodes, edges }, { svg });

    expect(svg.selectAll('circle').size()).toBe(2);
    expect(svg.selectAll('line').size()).toBe(1);
  });

  // Ajoutez ici d'autres tests pour les différentes options de configuration
});
