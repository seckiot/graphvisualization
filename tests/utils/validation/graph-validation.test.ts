/**
 * @jest-environment jsdom
 */

import { graphDataValidation } from '../../../src/utils/validation/graph-validation';

describe('graphDataValidation', () => {
  it('should throw an error if data is not an object', () => {
    expect(() => graphDataValidation(null)).toThrow(
      'The JSON data is not valid',
    );
    expect(() => graphDataValidation('string')).toThrow(
      'The JSON data is not valid',
    );
  });

  it('should throw an error if nodes are missing or not an array', () => {
    const data = {};
    expect(() => graphDataValidation(data)).toThrow(
      'Nodes are missing or not an array.',
    );
  });

  it('should throw an error if node structure is invalid', () => {
    const data = {
      nodes: [
        { id: 123, label: 'Node 1', color: 'blue', size: 10 }, // id is not a string
        { id: '2', label: 'Node 2', color: 'red', size: 'large' }, // size is not a number
      ],
      edges: [],
    };
    expect(() => graphDataValidation(data)).toThrow('Invalid node structure');
  });

  it('should throw an error if edges are missing or not an array', () => {
    const data = { nodes: [] };
    expect(() => graphDataValidation(data)).toThrow(
      'Edges are missing or not an array.',
    );
  });

  it('should throw an error if edge structure is invalid', () => {
    const data = {
      nodes: [],
      edges: [
        {
          id: 123,
          source: 'Node 1',
          target: 'Node 2',
          color: 'blue',
          size: 10,
        }, // id is not a string
        {
          id: '2',
          source: 'Node 1',
          target: 'Node 2',
          color: 'red',
          size: 'large',
        }, // size is not a number
      ],
    };
    expect(() => graphDataValidation(data)).toThrow('Invalid edge structure');
  });

  it('should throw an error if node label is not a string', () => {
    const data = {
      nodes: [
        { id: '1', label: 123, color: 'blue', size: 10 }, // label is not a string
      ],
      edges: [],
    };
    expect(() => graphDataValidation(data)).toThrow(
      'Invalid node structure: label should be a string',
    );
  });

  it('should throw an error if node color is not a string', () => {
    const data = {
      nodes: [
        { id: '1', label: 'Node 1', color: true, size: 10 }, // color is not a string
      ],
      edges: [],
    };
    expect(() => graphDataValidation(data)).toThrow(
      'Invalid node structure: color should be a string',
    );
  });

  it('should throw an error if edge source is not a string', () => {
    const data = {
      nodes: [],
      edges: [
        { id: '1', source: 123, target: 'Node 2', color: 'blue', size: 10 }, // source is not a string
      ],
    };
    expect(() => graphDataValidation(data)).toThrow(
      'Invalid edge structure: source should be a string',
    );
  });

  it('should throw an error if edge target is not a string', () => {
    const data = {
      nodes: [],
      edges: [
        { id: '1', source: 'Node 1', target: 123, color: 'blue', size: 10 }, // target is not a string
      ],
    };
    expect(() => graphDataValidation(data)).toThrow(
      'Invalid edge structure: target should be a string',
    );
  });

  it('should throw an error if edge color is not a string', () => {
    const data = {
      nodes: [],
      edges: [
        { id: '1', source: 'Node 1', target: 'Node 2', color: true, size: 10 }, // color is not a string
      ],
    };
    expect(() => graphDataValidation(data)).toThrow(
      'Invalid edge structure: color should be a string',
    );
  });

  it('should throw an error if edge size is not a string', () => {
    const data = {
      nodes: [],
      edges: [
        {
          id: '1',
          source: 'Node 1',
          target: 'Node 2',
          color: 'white',
          size: '10',
        }, // color is not a string
      ],
    };
    expect(() => graphDataValidation(data)).toThrow(
      'Invalid edge structure: size should be a number',
    );
  });

  it('should not throw an error for valid data', () => {
    const data = {
      nodes: [
        { id: '1', label: 'Node 1', color: 'blue', size: 10 },
        { id: '2', label: 'Node 2', color: 'red', size: 20 },
      ],
      edges: [
        { id: '1', source: 'N 1', target: 'N 2', color: 'blue', size: 10 },
        { id: '2', source: 'N 2', target: 'N 1', color: 'red', size: 20 },
      ],
    };
    expect(() => graphDataValidation(data)).not.toThrow();
  });
});
