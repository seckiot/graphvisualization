import * as renderer from 'react-test-renderer';
import { act } from 'react-dom/test-utils'; // import act
import FileInput from '../../../../src/components/ui/file-input/index';

describe('FileInput component', () => {
  it('renders correctly', () => {
    const onFileReadMock = jest.fn();
    const tree = renderer
      .create(<FileInput accept="image/*" onFileRead={onFileReadMock} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('calls onFileRead when a file is selected', () => {
    const onFileReadMock = jest.fn();
    const component = renderer.create(
      <FileInput accept=".json" onFileRead={onFileReadMock} />,
    );
    const instance = component.root;
    const input = instance.findByType('input');
    act(() => {
      // wrap the state update in act
      input.props.onChange({
        target: {
          files: [
            new File(['file'], 'file.json', { type: 'application/json' }),
          ],
        },
      });
    });
    expect(onFileReadMock).toHaveBeenCalled();
  });
});
