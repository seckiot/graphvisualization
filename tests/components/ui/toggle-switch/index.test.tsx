import * as renderer from 'react-test-renderer';
import { act } from 'react-dom/test-utils'; // import act
import ToggleSwitch from '../../../../src/components/ui/toggle-switch/index';

describe('ToggleSwitch component', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<ToggleSwitch id="test-switch" content="Test" />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with additional className', () => {
    const tree = renderer
      .create(
        <ToggleSwitch
          id="test-switch"
          content="Test"
          className="extra-class"
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('calls onChange when toggled', () => {
    const onChangeMock = jest.fn();
    const component = renderer.create(
      <ToggleSwitch id="test-switch" content="Test" onChange={onChangeMock} />,
    );
    const instance = component.root;
    const input = instance.findByType('input');
    act(() => {
      // wrap the state update in act
      input.props.onChange({ target: { checked: true } });
    });
    expect(onChangeMock).toHaveBeenCalled();
  });
});
