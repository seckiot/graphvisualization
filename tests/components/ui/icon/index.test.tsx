import * as renderer from 'react-test-renderer';
import Icon from '../../../../src/components/ui/icon/index';

describe('Icon component', () => {
  it("renders correctly with 'upload' icon", () => {
    const tree = renderer.create(<Icon icon="upload" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders correctly with 'upload' icon and additional className", () => {
    const tree = renderer
      .create(<Icon icon="upload" className="extra-class" />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders nothing for unknown icon', () => {
    const tree = renderer.create(<Icon icon="unknown" />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
