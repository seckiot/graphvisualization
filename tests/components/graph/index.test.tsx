import * as renderer from 'react-test-renderer';
import Graph from '../../../src/components/graph/index';
import { GraphProvider } from '../../../src/contexts/graph-context';
import { act } from 'react-dom/test-utils';

describe('Graph Component', () => {
  it('renders without crashing', () => {
    renderer.create(
      <GraphProvider>
        <Graph />
      </GraphProvider>,
    );
  });

  it('toggles node size switch', async () => {
    const testRenderer = renderer.create(
      <GraphProvider>
        <Graph />
      </GraphProvider>,
    );
    const testInstance = testRenderer.root;

    const nodeSizeSwitch = testInstance.findByProps({ id: 'nodeSizeToggle' });

    expect(nodeSizeSwitch.props.checked).toBe(false);

    // Simulate a click event

    await act(async () => {
      nodeSizeSwitch.props.onChange();
    });

    expect(nodeSizeSwitch.props.checked).toBe(true);
  });

  it('toggles edge size switch', async () => {
    const testRenderer = renderer.create(
      <GraphProvider>
        <Graph />
      </GraphProvider>,
    );
    const testInstance = testRenderer.root;

    const edgeSizeSwitch = testInstance.findByProps({ id: 'edgeSizeToggle' });
    expect(edgeSizeSwitch.props.checked).toBe(false);

    // Simulate a click event

    await act(async () => {
      edgeSizeSwitch.props.onChange();
    });

    expect(edgeSizeSwitch.props.checked).toBe(true);
  });
});
