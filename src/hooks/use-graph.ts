import { useContext } from 'react';
import { GraphContext } from '../contexts/graph-context';
// Custom hook to use the GraphContext
export function useGraph() {
  const context = useContext(GraphContext);
  if (!context) {
    throw new Error('useGraph must be used inside a GraphProvider');
  }
  return context;
}
