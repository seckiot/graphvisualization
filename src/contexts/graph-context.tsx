// GraphContext.tsx

// Creating a context to manage the state of the JSON data
// These data may need to be used in other components than the graph if the project becomes more complex
import { createContext, useReducer } from 'react';
import { GraphAction, GraphState } from '../models/graph-types';

// Initial state
const initialState: GraphState = {
  nodes: [],
  edges: [],
};

type GraphProviderProps = { children: React.ReactNode };

// Creating the context with the initial state
export const GraphContext = createContext<{
  graphState: GraphState;
  dispatch: React.Dispatch<GraphAction>;
}>({ graphState: initialState, dispatch: () => null });

// Reducer to handle actions
export const graphReducer = (state: GraphState, action: GraphAction) => {
  switch (action.type) {
    case 'SET_GRAPH_DATA':
      return {
        ...state,
        nodes: action.payload.nodes,
        edges: action.payload.edges,
      };
    default:
      return state;
  }
};

// Creating a provider
export const GraphProvider = ({ children }: GraphProviderProps) => {
  const [graphState, dispatch] = useReducer(graphReducer, initialState);

  return (
    <GraphContext.Provider value={{ graphState, dispatch }}>
      {children}
    </GraphContext.Provider>
  );
};
