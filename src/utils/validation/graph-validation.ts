//valiation.tsx
import { EdgeType, NodeType } from '../../models/graph-types.ts';

export const graphDataValidation = (data: any) => {
  if (!data || typeof data !== 'object') {
    throw new Error('The JSON data is not valid');
  }

  if (!data.nodes || !Array.isArray(data.nodes)) {
    throw new Error('Nodes are missing or not an array.');
  } else {
    data.nodes.forEach((node: NodeType) => {
      if (typeof node.id !== 'string') {
        throw new Error(
          'Invalid node structure: id should be a string at node.id = ' +
            node.id,
        );
      }
      if (typeof node.label !== 'string') {
        throw new Error(
          'Invalid node structure: label should be a string at node.id = ' +
            node.id,
        );
      }
      if (typeof node.color !== 'string') {
        throw new Error(
          'Invalid node structure: color should be a string at node.id = ' +
            node.id,
        );
      }
      if (typeof node.size !== 'number') {
        throw new Error(
          'Invalid node structure: size should be a number at node.id = ' +
            node.id,
        );
      }
    });
  }

  if (!data.edges || !Array.isArray(data.edges)) {
    throw new Error('Edges are missing or not an array.');
  } else {
    data.edges.forEach((edge: EdgeType) => {
      if (typeof edge.id !== 'string') {
        throw new Error(
          'Invalid edge structure: id should be a string at edge.id = ' +
            edge.id,
        );
      }
      if (typeof edge.source !== 'string') {
        throw new Error(
          'Invalid edge structure: source should be a string at edge.id = ' +
            edge.id,
        );
      }
      if (typeof edge.target !== 'string') {
        throw new Error(
          'Invalid edge structure: target should be a string at edge.id = ' +
            edge.id,
        );
      }
      if (typeof edge.color !== 'string') {
        throw new Error(
          'Invalid edge structure: color should be a string at edge.id = ' +
            edge.id,
        );
      }
      if (typeof edge.size !== 'number') {
        throw new Error(
          'Invalid edge structure: size should be a number at edge.id = ' +
            edge.id,
        );
      }
    });
  }
};
