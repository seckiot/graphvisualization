// Copyright 2021-2023 Observable, Inc.
// Released under the ISC license.
// https://observablehq.com/@d3/force-directed-graph
import { NodeType, EdgeType, AttributeType } from '@models/graph-types';
import * as d3 from 'd3';

interface ForceGraphOptions {
  svg?: d3.Selection<SVGSVGElement, undefined, null, undefined>;
  nodeId?: (d: NodeType) => string;
  nodeColor?: (d: NodeType) => string;
  nodeSize?: (d: NodeType) => number;
  nodeAttribute?: (d: NodeType) => AttributeType;
  nodeGroup?: (d: NodeType) => string;
  nodeGroups?: string[];
  nodeTitle?: (d: NodeType) => string | undefined;
  nodeFill?: string;
  nodeStroke?: string;
  nodeStrokeWidth?: number;
  nodeStrokeOpacity?: number;
  nodeRadius?: number;
  nodeStrength?: number;
  linkSource?: (d: EdgeType) => string;
  linkTarget?: (d: EdgeType) => string;
  linkStroke?: string | ((d: EdgeType) => string);
  linkStrokeOpacity?: number;
  linkStrokeWidth?: number | ((d: EdgeType) => number);
  linkStrokeLinecap?: string;
  linkStrength?: (d: EdgeType) => number;
  width?: number;
  height?: number;
  invalidation?: Promise<any>;
}

export function forceGraph(
  {
    nodes, // an iterable of node objects (typically [{id}, …])
    edges, // an iterable of link objects (typically [{source, target}, …])
  }: { nodes: NodeType[]; edges: EdgeType[] },
  {
    svg,
    nodeId = (d) => d.id, // given d in nodes, returns a unique identifier (string)
    nodeColor, // given d in nodes, returns a color (string)
    nodeSize, // given d in nodes, returns a size (number)
    nodeAttribute, // given d in nodes, returns a size (number)
    nodeGroup, // given d in nodes, returns an (ordinal) value for color
    nodeGroups, // an array of ordinal values representing the node groups
    nodeTitle, // given d in nodes, a title string
    nodeFill = 'currentColor', // node stroke fill (if not using a group color encoding)
    nodeStroke = '#fff', // node stroke color
    nodeStrokeWidth = 1.5, // node stroke width, in pixels
    nodeStrokeOpacity = 1, // node stroke opacity
    nodeRadius = 5, // node radius, in pixels
    nodeStrength,
    linkSource = ({ source }) => source, // given d in edges, returns a node identifier string
    linkTarget = ({ target }) => target, // given d in edges, returns a node identifier string
    linkStroke = '#999', // link stroke color
    linkStrokeOpacity = 0.6, // link stroke opacity
    linkStrokeWidth = 1.5, // given d in edges, returns a stroke width in pixels
    linkStrokeLinecap = 'round', // link stroke linecap
    linkStrength,
    width = 640, // outer width, in pixels
    height = 400, // outer height, in pixels
    invalidation, // when this promise resolves, stop the simulation
  }: ForceGraphOptions = {},
): { zoom: d3.ZoomBehavior<SVGSVGElement, undefined> } {
  // Compute values.
  const N = d3.map(nodes, nodeId).map(intern);
  const NC = typeof nodeColor !== 'function' ? null : d3.map(nodes, nodeColor);
  const NS = typeof nodeSize !== 'function' ? null : d3.map(nodes, nodeSize);
  const NA =
    typeof nodeAttribute !== 'function' ||
    !nodes.some((node) => nodeAttribute(node))
      ? null
      : d3.map(nodes, nodeAttribute);
  if (nodeTitle === undefined) nodeTitle = (d) => d.id;
  const T = nodeTitle == null ? null : d3.map(nodes, nodeTitle);
  const G = nodeGroup == null ? null : d3.map(nodes, nodeGroup).map(intern);
  const W =
    typeof linkStrokeWidth !== 'function'
      ? null
      : d3.map(edges, linkStrokeWidth);
  const L = typeof linkStroke !== 'function' ? null : d3.map(edges, linkStroke);

  // Replace the input nodes and edges with mutable objects for the simulation.
  nodes = d3.map(nodes, (node) => ({ id: nodeId(node) }));
  edges = d3.map(edges, (edge) => ({
    source: linkSource(edge),
    target: linkTarget(edge),
  })) as d3.SimulationLinkDatum<any>[];

  // Compute default domains.
  if (G && nodeGroups === undefined) nodeGroups = d3.sort(G);

  // Construct the forces.
  const forceNode = d3.forceManyBody();
  const forceLink = d3.forceLink(edges).id(({ index }) => N[index!]);

  if (nodeStrength !== undefined) forceNode.strength(nodeStrength);
  if (linkStrength !== undefined) forceLink.strength(linkStrength);

  const simulation = d3
    .forceSimulation(nodes as d3.SimulationNodeDatum[])
    .force('link', forceLink)
    .force('charge', forceNode)
    .force('center', d3.forceCenter())
    .on('tick', ticked);

  simulation.tick(15);

  svg!
    .attr('width', width)
    .attr('height', height)
    .attr('viewBox', [-width / 2, -height / 2, width, height]);

  const link = svg!
    .append('g')
    .attr('stroke', typeof linkStroke !== 'function' ? linkStroke : null)
    .attr('stroke-opacity', linkStrokeOpacity)
    .attr(
      'stroke-width',
      typeof linkStrokeWidth !== 'function' ? linkStrokeWidth : null,
    )
    .attr('stroke-linecap', linkStrokeLinecap)
    .selectAll('line')
    .data(edges)
    .join('line');

  const node = svg!
    .append('g')
    .attr('fill', nodeFill)
    .attr('stroke', nodeStroke)
    .attr('stroke-opacity', nodeStrokeOpacity)
    .attr('stroke-width', nodeStrokeWidth)
    .selectAll('circle')
    .data(nodes)
    .join('circle')
    .attr('r', nodeRadius)
    .call(
      //@ts-expect-error unknow
      d3
        .drag()
        .on('start', dragStarted)
        .on('drag', dragged)
        .on('end', dragEnded),
    );

  if (W) link.attr('stroke-width', ({ index: i }) => W[i!]);
  if (L) link.attr('stroke', ({ index: i }) => L[i!]);
  if (NC) node.attr('fill', ({ index: i }) => NC[i!]);
  if (NS) node.attr('r', ({ index: i }) => NS[i!]);
  if (T) node.append('title').text(({ index: i }) => T[i!]!);
  if (N) node.attr('id', ({ index: i }) => i!);
  if (invalidation != null) invalidation.then(() => simulation.stop());

  function intern(value: any) {
    return value !== null && typeof value === 'object'
      ? value.valueOf()
      : value;
  }

  function isNodeInViewport(
    node: d3.SimulationNodeDatum,
    transform: d3.ZoomTransform,
  ) {
    const { x, y } = node;
    const newX = transform.applyX(x!);
    const newY = transform.applyY(y!);
    const { width, height } = svg!.node()!.getBoundingClientRect();
    return (
      newX >= -width / 2 &&
      newX <= width / 2 &&
      newY >= -height / 2 &&
      newY <= height / 2
    );
  }

  function isLinkInViewport(
    link: d3.SimulationLinkDatum<any>,
    transform: d3.ZoomTransform,
  ) {
    return (
      isNodeInViewport(link.source, transform) ||
      isNodeInViewport(link.target, transform)
    );
  }

  function ticked() {
    const transform = d3.zoomTransform(svg!.node() as unknown as Element);
    link
      .attr('x1', (d: d3.SimulationLinkDatum<any>) => d.source.x)
      .attr('y1', (d: d3.SimulationLinkDatum<any>) => d.source.y)
      .attr('x2', (d: d3.SimulationLinkDatum<any>) => d.target.x)
      .attr('y2', (d: d3.SimulationLinkDatum<any>) => d.target.y)
      .style('display', (d) =>
        isLinkInViewport(d, transform) ? null : 'none',
      ); // Affiche seulement les liens visibles

    node
      .attr('cx', (d: any) => d.x)
      .attr('cy', (d: any) => d.y)
      .style('display', (d) =>
        isNodeInViewport(d as d3.SimulationNodeDatum, transform)
          ? null
          : 'none',
      ); // Afficher seulement si dans le viewport
  }

  function dragStarted(event: any) {
    if (!event.active) simulation.alphaTarget(0.3).restart();
    event.subject.fx = event.subject.x;
    event.subject.fy = event.subject.y;
    node.attr('cursor', 'grabbing');
  }

  function dragged(event: any) {
    event.subject.fx = event.x;
    event.subject.fy = event.y;
  }

  function dragEnded(event: any) {
    if (!event.active) simulation.alphaTarget(0);
    event.subject.fx = null;
    event.subject.fy = null;
    node.attr('cursor', 'grab');
  }

  // Create zoom behavior
  const zoom = d3
    .zoom<SVGSVGElement, undefined>()
    .scaleExtent([0.1, 5]) // Set the zoom scale limits
    .on('zoom', (event) => {
      const { transform } = event;
      svg!.selectAll('g').attr('transform', transform);
      link.style('display', (d) =>
        isLinkInViewport(d, transform) ? null : 'none',
      );
      node.style('display', (d) =>
        isNodeInViewport(d as d3.SimulationNodeDatum, transform)
          ? null
          : 'none',
      );
    });

  // Apply zoom behavior to the SVG element
  zoom(svg as unknown as d3.Selection<SVGSVGElement, undefined, any, any>);

  // Define the div for the tooltip
  const tip = d3
    .select('body')
    .append('div')
    .attr('class', 'tooltip')
    .style('display', 'none');
  // Add events to circles
  node
    .on('mouseover', function (event) {
      const x = event.target.getBoundingClientRect().x;
      const y = event.target.getBoundingClientRect().y;

      tip
        .style('display', 'block')
        .style('left', x + 15 + 'px')
        .style('top', y - 35 + 'px')
        .html('<b>' + T![event.target.id] + '</b><br>');

      if (NA) {
        const attributeEntries = Object.entries(NA[event.target.id]);
        attributeEntries.forEach(([key, value]) => {
          tip
            .append('span')
            .attr('class', 'italic')
            .html('<b>' + key + '</b>: ' + value);
        });
      }
    })
    .on('mouseout', function () {
      tip.style('display', 'none');
    });

  return { zoom };
}
