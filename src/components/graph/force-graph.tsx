import { useEffect, useRef } from 'react';
import * as d3 from 'd3';
import { forceGraph } from '../../utils/graph/force-graph';
import { useGraph } from '../../hooks/use-graph';
import { NodeType, EdgeType, AttributeType } from '../../models/graph-types';

type ForceGraphProps = {
  isNodeSize: boolean;
  isEdgeSize: boolean;
};

const isSmartphone = window.innerWidth <= 480;
console.log(isSmartphone);
const width = isSmartphone ? window.innerWidth-65 : window.innerWidth-200;
const height = isSmartphone ? width : 600;

function ForceGraph({ isNodeSize, isEdgeSize }: ForceGraphProps) {
  const { graphState } = useGraph();
  const graphRef = useRef<SVGSVGElement>(null);

  // This effect runs whenever the graphState, isNodeSize, or isEdgeSize changes.
  useEffect(() => {
    if (graphState.nodes.length > 0 && graphState.edges.length > 0) {
      const svg = d3.select<SVGSVGElement, undefined>(graphRef.current!);
      svg.selectAll('*').remove(); // Clear previous content

      const graphParam = {
        svg,
        nodeTitle: (d: NodeType): string | undefined => d.label,
        nodeColor: (d: NodeType): string => d.color ?? 'fff',
        nodeAttribute: (d: NodeType): AttributeType => d.attributes!,
        nodeSize: isNodeSize ? (d: NodeType): number => d.size ?? 5 : undefined,
        linkStroke: (d: EdgeType): string => d.color ?? '#fff',
        linkStrokeWidth: isEdgeSize
          ? (d: EdgeType): number => d.size ?? 1.5
          : undefined,
        width: width,
        height: height,
      };

      const { zoom } = forceGraph(graphState, graphParam); // Generate SVG elements using D3

      return () => {
        svg.selectAll('*').remove(); // Clear previous content
        svg.call(zoom.transform, d3.zoomIdentity); // reset zoom
      };
    }
  }, [graphState, isNodeSize, isEdgeSize]);

  return (
    <svg ref={graphRef} width={width} height={height} className="graphSvg">
      <text
        x="50%"
        y="50%"
        dominantBaseline="middle"
        textAnchor="middle"
        className="fill-white"
      >
        Please upload a JSON
      </text>
    </svg>
  );
}

export default ForceGraph;
