import { useState } from 'react';
import { toast } from 'react-toastify';
import { useGraph } from '../../hooks/use-graph';
import { graphDataValidation } from '../../utils/validation/graph-validation';

import ForceGraph from './force-graph';
import ToggleSwitch from '../../components/ui/toggle-switch';
import FileInput from '../../components/ui/file-input';

function Graph() {
  const [isNodeSize, setIsNodeSize] = useState<boolean>(false);
  const [isEdgeSize, setisEdgeSize] = useState<boolean>(false);
  const { dispatch } = useGraph();

  // This function is responsible for reading the file and processing its data.
  const handleFileRead = (file: File, fileReader: FileReader) => {
    fileReader.readAsText(file, 'UTF-8');
    fileReader.onload = (e) => {
      try {
        const json = e.target?.result
          ? JSON.parse(e.target?.result.toString())
          : null;
        // Validate the JSON data against some predefined schema or requirements.
        graphDataValidation(json);

        // If the validation is successful, we dispatch an action to update the state with the new graph data.
        dispatch({ type: 'SET_GRAPH_DATA', payload: json });
      } catch (error) {
        console.error('Error while reading or validating the JSON file', error);
        //show a toast notification with the error message.
        toast.error((error as Error).message);
      }
    };
  };

  return (
    <div className="flex flex-col md:flex-row gap-2">
      <div className="flex flex-col items-start gap-4">
        <FileInput accept=".json" onFileRead={handleFileRead} />
        <ToggleSwitch
          id="nodeSizeToggle"
          content="Node Size"
          checked={isNodeSize}
          onChange={() => setIsNodeSize(!isNodeSize)}
        />
        <ToggleSwitch
          id="edgeSizeToggle"
          content="Edge Size"
          checked={isEdgeSize}
          onChange={() => setisEdgeSize(!isEdgeSize)}
        />
      </div>
      <div className="flex flex-col overflow-hidden md:w-[800px] justify-center items-center">
        <ForceGraph isNodeSize={isNodeSize} isEdgeSize={isEdgeSize} />
      </div>
    </div>
  );
}

export default Graph;
