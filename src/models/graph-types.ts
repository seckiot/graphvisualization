//graph-types.ts
export type NodeType = {
  id: string;
  index?: number;
  label?: string;
  attributes?: AttributeType;
  color?: string;
  size?: number;
};

export type AttributeType = {
  [key: string]: string | number | boolean;
};

export type EdgeType = {
  source: string;
  target: string;
  id?: string;
  index?: number;
  attributes?: AttributeType;
  color?: string;
  size?: number;
};

// Définition du export type pour les données du graphe
export type GraphState = {
  nodes: NodeType[];
  edges: EdgeType[];
};

// Création du export type d'action pour le reducer
export type GraphAction = {
  type: string;
  payload: GraphState;
};
