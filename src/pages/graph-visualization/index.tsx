import Graph from '../../components/graph';
import { ToastContainer } from 'react-toastify';
import { GraphProvider } from '../../contexts/graph-context';

function GraphVisualization() {
  return (
    <GraphProvider>
      <h1 className="mb-12 text-left">Graph Visualization</h1>
      <Graph />
      <ToastContainer />
    </GraphProvider>
  );
}

export default GraphVisualization;
