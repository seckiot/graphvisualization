import 'react-toastify/dist/ReactToastify.css';
import './css/app.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import GraphVisualization from './pages/graph-visualization';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<GraphVisualization />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
