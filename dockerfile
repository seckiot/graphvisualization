# Étape de construction
# Utilisez la version 20.11.0 de Node.js comme base
FROM node:20.11-alpine3.18 as build

# This tells Docker to create a directory so that other commands will use it
WORKDIR /app

#Copy Our package.json and package-lock.json file into the app directory to tell node the module we want to use
COPY package.json /app

#To install the dependencies inside our image

RUN npm install

# Copy everything from ourlocal directory to the image in the code directory
COPY . /app

# build production environment
RUN npm run build

# Navigate to the application entry point and run the image
CMD [ "npm", "run", "preview" ]