# GraphVisualization

## Introduction

GraphVisualization is a React application that offers a unique and interactive way to visualize graph data. This application allows users to upload JSON files containing graph data (nodes and links) and renders this data as a force-directed graph using d3.js. It comes with features like zoom and panning to enhance the user experience.

## Prerequisites

Before you start, make sure you have the following tools installed:

- **Node.js** : Version 20.11.0.
- **npm** : Package manager for JavaScript.
- **Git** : Version control system, necessary for cloning the repository.

## Installation

To set up and run GraphVisualization on your local machine, follow these steps:

1. **Clone the GitLab Repository** :
   Open a terminal and execute the following command to clone the project :
   `git clone git@gitlab.com:seckiot/graphvisualization.git`
2. **Access the Project Folder** :
   Once the cloning is complete, navigate to the project folder :
   `cd graphvisualization`
3. **Install Dependencies** :
   Install all required dependencies by executing :
   `npm install`
4. **Launch the Application** :
   After installation, start the application in development mode :
   `npm run dev`
5. **Optional: Launch the Application in production mode with Docker Compose** :
   If you have Docker installed, you can use Docker Compose to start the application. This will create a container for your application and manage all the necessary dependencies for you. 
   
   To do this, run the following command in the terminal:
   `docker-compose up --build --no-recreate -d`

## State Management and Business Logic

In GraphVisualization, the state management of the graph data is handled using the Context API, a feature of React. While managing state with `useState` would have been simpler for smaller applications, the decision to use Context API was driven by the anticipation of increased complexity in the future.

This choice also provided an opportunity to demonstrate my proficiency with the Context API. It illustrates a more advanced state management approach that could be beneficial as the application evolves, accommodating more complex interactions and data flows.

## Project Architecture

The `GraphVisualization` project is structured to promote ease of maintenance, scalability, and clarity. Here's an overview of the main directories and files:

- **`miserables/`**: Contains all the necessary files for testing the application.

- **`src/`**: The source directory contains all the core code for the application.

  - **`assets/`**: This folder is intended for static files like images, fonts, and other assets that the application may require.

  - **`components/`**: Contains all React components, further divided into:

    - **`graph/`**: Includes components specifically related to the graph visualization aspect of the application.
    - **`ui/`**: General UI components that can be reused throughout the application.

  - **`contexts/`**: Holds the Context definitions for the application.

  - **`css/`**: Contains the CSS files for styling the application.

  - **`hooks/`**: Custom React hooks that encapsulate reusable logic.

  - **`models/`**: Defines TypeScript interfaces or types.

  - **`pages/graph-visualization`**: Represents the page components.

  - **`utils/`**: A collection of utility functions that help with various tasks.

    - **`graph/`**: subdirectory with utility functions like force-graph.ts for the force-directed graph logic
    - **`validation/`** subdirectory with dat validation functions.

- **`tests/`**: Contains all the test files, categorized similarly to the src directory, ensuring that components, contexts, hooks, pages, and utilities have their corresponding tests.

## Technologies Used

The `GraphVisualization` project leverages a modern JavaScript stack for efficient and maintainable development:

- **React**: A declarative, component-based library for building user interfaces.
- **TypeScript**: A typed superset of JavaScript that compiles to plain JavaScript, enhancing the development experience with type safety.
- **Tailwind CSS**: A utility-first CSS framework for rapidly building custom designs without leaving your HTML.
- **ESLint**: A static code analysis tool for identifying problematic patterns in JavaScript code.
- **Prettier**: An opinionated code formatter that enforces a consistent style by parsing code and re-printing it with its own rules.
- **Vite**: A modern frontend build tool that significantly improves the development experience, offering features like instant server start and hot module replacement.
- **Jest**: A JavaScript testing framework with a focus on simplicity, used for writing unit and integration tests.
- **React Testing Library**: A set of helpers that allow you to test React components without relying on their implementation details, favoring user behavior-based testing.

This combination of technologies provides a robust platform for building and testing the application with a focus on developer ergonomics and best practices.

# Running Tests

This project uses Jest as its testing framework. To run the tests, you can use the following command in your terminal:

`npm run test`

# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh
